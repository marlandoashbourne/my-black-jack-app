(function()
{


    var AppCtrl =  function($scope, $timeout,  $interval)
    {
        $scope.player       = 0;
        $scope.dealer       = 0;
        $scope.num          = 0;
        $scope.playerStat   = "";
        $scope.playerWin    = false;
        $scope.dealerWin    = false;
        $scope.dealerStat   = "";
        $scope.disabled     = false; //Disable's hit and stand buttons
        $scope.game         = true;
        $scope.dealerHand   = [];
        $scope.playerHand   = [];
        $scope.hits         = 0;
        $scope.state        = "Deal";

        // Player's Hits
        $scope.hit = function()
        {
            console.log("Hit Press");
            $scope.state = "Hit";
            //Deal Player gets 2 Cards on the first Hit
            if($scope.hits == 0)
            {
                console.log('Deal');
                $scope.num = Math.floor((Math.random() * 11) + 1);
                $scope.playerHand.push($scope.num);
                $scope.player = $scope.player + $scope.num;
                
                $scope.num = Math.floor((Math.random() * 11) + 1);
                $scope.playerHand.push($scope.num);
                $scope.player = $scope.player + $scope.num;


            }
            else
            {
                console.log('Hit');
                $scope.num = Math.floor((Math.random() * 11) + 1);
                $scope.playerHand.push($scope.num);

                console.log($scope.num);
                $scope.player = $scope.player + $scope.num;
            }

            $scope.hits = $scope.hits +1;
            //If Player Bust
            if($scope.player > 21)
            {
                $scope.playerWin    = false;
                $scope.dealerWin    = true;
                $scope.disabled = true;
                $scope.game     = false;

            }

            //If Player Gets Exactly 21 
            if($scope.player == 21)
            {                   
                $scope.playerWin    = true;
                $scope.dealerWin    = false;
                $scope.disabled = true;
                $scope.game =false;


            }
            
        };


         $scope.stand = function()
        {
            console.log("Stand Press");

            $scope.disabled = true;
            $scope.num = 0;

            var myVar;


            promise = $interval(dealerPlay, 1000); //Dealer takes card every second
            
        };

        //Dealer's Play When Player Stand
        function dealerPlay() 
        {
          // items to randomize 1 - 11
            $scope.num = Math.floor((Math.random() * 11) + 1);
            $scope.dealerHand.push($scope.num);

            $scope.dealer = $scope.dealer + $scope.num;
              
                //If Dealer Bust        
            if ($scope.dealer >21) 
            {
                $scope.playerWin    = true;    
                $scope.dealerWin    = false;
                $interval.cancel(promise);
                $scope.game =false;
                $scope.disabled = true;

            }
                //If Dealer Gets Exactly 21        
            else if($scope.dealer  == 21)
            {
                // $scope.dealerStat ="Win";
                // $scope.playerStat ="You Lost";
                $scope.playerWin    = false;
                $scope.dealerWin    = true;
                $interval.cancel(promise);
                $scope.game = false;
                $scope.disabled = true;



            }
                //If Dealer Gets 17 Or Over        
            else if($scope.dealer >=17)
            {
                $interval.cancel(promise);
                $scope.game = true;
                $scope.disabled = true;

                 console.log('Dealer 17 or higer');

                  if($scope.dealer > $scope.player)
                    {
                        // $scope.dealerStat ="Wins";
                        // $scope.playerStat ="Lost";
                        $scope.playerWin    = false;
                        $scope.dealerWin    = true;
                        $scope.disabled     = true;


                    }
                    else
                    {
                        $scope.playerWin    = true;
                        $scope.dealerWin    = false;
                        $scope.disabled     = true;

                    }

            }
            console.log($scope.playerWin);

        };

        //Reset Values 
        $scope.reset = function()
        {
            $scope.num    = 0;
            $scope.player = 0;
            $scope.dealer = 0;
            $scope.num    = 0;
            $scope.hits   = 0;
            $scope.disabled = false;
            $scope.dealerHand = [];
            $scope.playerHand = [];
            $scope.playerStat = "";
            $scope.dealerStat = "";
            $scope.state      = "Deal";
            $scope.playerWin    = false;
            $scope.dealerWin    = false;

         


        };
    };


    AppCtrl.$inject = ['$scope', '$timeout', '$interval'];

    angular.module('blackJackApp').controller('AppCtrl', AppCtrl);


}());